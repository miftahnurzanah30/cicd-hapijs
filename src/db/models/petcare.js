class PetCare {
    constructor(id, services, price) {
        this.id = id;
        this.services = services;
        this.price = price;
    }
}
export default PetCare;