"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _landing = _interopRequireDefault(require("./landing"));

var _customers = _interopRequireDefault(require("./customers"));

var _petcares = _interopRequireDefault(require("./petcares"));

var routes = [].concat(_landing["default"], _customers["default"], _petcares["default"]);
var _default = routes;
exports["default"] = _default;