import PetcareService from "../../services/petcare.service";
import Boom from '@hapi/boom';
import Joi from '@hapi/joi';

const service = new PetcareService();

const petcare = [{
        method: 'GET',
        path: '/petcares',
        handler: async(request, h) => {
            return await service.findAll();
        },
    },
    {
        method: 'GET',
        path: '/petcare/{id?}',
        handler: async(req, h) => {
            const { id } = req.params;
            const petcare = await service.findOne({ id });

            if (!petcare) {
                throw Boom.notFound('petcare not found')
            } else {
                return petcare;
            }
        }
    },
    {
        method: 'POST',
        path: '/petcares',
        config: {
            handler: async(req, h) => {
                const data = await service.save(req.payload);
                return h.response({
                    data
                }).code(201);
            },
            validate: {
                payload: {
                    services: Joi.string().required(),
                    price: Joi.number().required(),
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/petcare/{id?}',
        handler: async(req, h) => {
            await service.delete(req.params.id);
            return h.response().code(204);
        }
    },
    {
        method: 'PUT',
        path: '/petcare/',
        handler: async(req, h) => {
            const petcare = req.payload;
            const data = await service.update(petcare);
            return h.response({
                data,
            });
        }
    }

];

export default petcare;