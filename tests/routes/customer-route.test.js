import request from 'supertest';
import init from '../../src/api';
import CustomerService from '../../src/services/customer.service';

const service = new CustomerService();

const payloads = [{
        "username": "miftah",
        "name": "miftah nurzanah",
        "address": "jakarta",
        "handphone": "021xxxxxxx",
        "password": "12345"
    },
    {
        "username": "nurz",
        "name": "miftah nurzanah",
        "address": "jakarta",
        "handphone": "021xxxxxxx",
        "password": "12345"
    },
    {
        "username": "zanah",
        "name": "miftah nurzanah",
        "address": "jakarta",
        "handphone": "021xxxxxxx",
        "password": "12345"
    }
]
let app;
describe('customers route', () => {

    beforeEach(async() => {
        app = await init();
    });

    it('GET /customers end point should return array of customer', async() => {
        let savedCustomers = [];

        payloads.forEach(async(element) => {
            savedCustomers.push(await service.save(element));
        })

        const response = await request(app).get('/customers').auth('miftah', '12345');
        const actual = await service.findAll();
        await expect(response.body).toMatchObject(service.findAll())
    });

    afterEach(async() => {
        if (app) {
            app.close();
        }
    })
})