"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var Transaction = function Transaction(id, date, total, layanan) {
  (0, _classCallCheck2["default"])(this, Transaction);
  this.id = id;
  this.date = date;
  this.total = total;
  this.layanan = layanan;
};

var _default = Transaction;
exports["default"] = _default;