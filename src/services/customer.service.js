import { getRepository as repository } from 'typeorm';
import Customer from '../db/models/customer';
import bcryptjs from 'bcryptjs';

// const bcryptjs = require(bcryptjs);

export default class CustomerService {
    customerService() {
        return new CustomerService;
    }

    customerRepository() {
        return repository(Customer);
    }

    findAll() {
        return this.customerRepository().find();
    }

    findOne(id) {
        return this.customerRepository().findOne(id);
    }

    findByUsername(username) {
        return this.customerRepository().findOne({ username });
    }

    // search(payload){
    //     if (payload.username) {
    //         return this.findByUsername(payload.username);
    //     }else if(payload.name){
    //         return this.customerRepository().find({ name : Like(`%${payload.name}%`)});
    //     }else if(payload.username && payload.name){
    //         return this.customerRepository().find({ username: Like(`%${payload.name}%`) })
    //     }
    // }

    async save(customer) {
        const { password } = customer;
        console.log(password);
        customer.password = await this.beforeCreate(password);
        console.log(customer.password);
        return this.customerRepository().save(customer);
    }

    delete(id) {
        return this.customerRepository().delete(id);
    }

    update(data) {
        return this.customerRepository().save(data);
    }

    async beforeCreate(password) {
        const salt = bcryptjs.genSaltSync();
        return await bcryptjs.hashSync(password, salt);
    }

    async validPassword(password, checkPassword) {
        return await bcryptjs.compareSync(password, checkPassword);
    }


}