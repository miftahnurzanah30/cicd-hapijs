"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _customer = _interopRequireDefault(require("./schemas/customer.schema"));

var _petcare = _interopRequireDefault(require("./schemas/petcare.schema"));

// import TransactionSchema from "./schemas/transaction.schema";
var _default = [_customer["default"], _petcare["default"] // TransactionSchema,
];
exports["default"] = _default;