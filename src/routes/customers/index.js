import CustomerService from "../../services/customer.service";
import Boom from '@hapi/boom';
import Joi from '@hapi/joi';

const service = new CustomerService();

const customer = [{
        method: 'GET',
        path: '/customers',
        config: {
            // auth: 'simple',
            handler: async(request, h) => {
                const customers = await service.findAll();
                return h.response({
                    customers
                });
            },
        }
    },
    {
        method: 'GET',
        path: '/customer/{id?}',
        handler: async(req, h) => {
            const { id } = req.params;
            const customer = await service.findOne({ id });

            if (!customer) {
                throw Boom.notFound('customer not found')
            } else {
                return customer;
            }
        }
    },
    {
        method: 'POST',
        path: '/customers',
        config: {
            handler: async(req, h) => {
                const data = await service.save(req.payload);
                return h.response({
                    data
                }).code(201);
            },
            validate: {
                query: Joi.object({
                    name: Joi.required(),
                    username: Joi.required(),
                    password: Joi.required(),
                    address: Joi.required()
                })
            }
        }
    },
    {
        method: 'DELETE',
        path: '/customer/{id?}',
        handler: async(req, h) => {
            await service.delete(req.params.id);
            return h.response().code(204);
        }
    },
    {
        method: 'PUT',
        path: '/customer/',
        config: {
            handler: async(req, h) => {
                const cust = req.payload;
                const data = await service.update(cust);
                return h.response({
                    data,
                });
            },
            validate: {
                query: Joi.object({
                    id: Joi.number().integer().min(1).required()
                })
            }
        },

    }

];

export default customer;