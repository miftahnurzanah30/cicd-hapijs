"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeorm = require("typeorm");

var _petcare = _interopRequireDefault(require("../models/petcare"));

var PetcareSchema = new _typeorm.EntitySchema({
  name: 'PetCare',
  target: _petcare["default"],
  tableName: 'petcares',
  columns: {
    id: {
      type: 'int',
      generated: true,
      nullable: false,
      primary: true
    },
    services: {
      type: 'varchar',
      length: 100,
      nullable: false
    },
    price: {
      type: 'int',
      nullable: false
    }
  } // relations: {
  //     layanan: {
  //         target: 'Transaction',
  //         type: 'many-to-one',
  //         joinColumn: true,
  //     },
  // }

});
var _default = PetcareSchema;
exports["default"] = _default;