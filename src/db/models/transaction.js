class Transaction {
    constructor(id, date, total, layanan) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.layanan = layanan;
    }
}

export default Transaction;