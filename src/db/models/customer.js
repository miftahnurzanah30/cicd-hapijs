class Customer {
    constructor(id, name, address, handphone, username, password) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.handphone = handphone;
        this.username = username;
        this.password = password;
    }
}

export default Customer;