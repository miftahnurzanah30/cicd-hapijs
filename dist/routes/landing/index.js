"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var landing = {
  method: 'GET',
  path: '/',
  handler: function handler(request, h) {
    return h.response({
      statusCode: 200,
      message: 'Hello World'
    });
  }
};
var _default = landing;
exports["default"] = _default;