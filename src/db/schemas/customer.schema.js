import { EntitySchema } from 'typeorm';
import Customer from '../models/customer';

const CustomerSchema = new EntitySchema({
    name: 'Customer',
    target: Customer,
    tableName: 'customers',
    columns: {
        id: {
            type: 'int',
            generated: true,
            nullable: false,
            primary: true,
        },
        name: {
            type: 'varchar',
            length: 100,
            nullable: false,
        },
        address: {
            type: 'varchar',
            length: 100,
            nullable: false,
        },
        handphone: {
            type: 'varchar',
            length: 100,
            nullable: false,
        },
        username: {
            type: 'varchar',
            length: 100,
            nullable: false,
        },
        password: {
            type: 'varchar',
            length: 255,
            nullable: false,
        }
    },
})

export default CustomerSchema;