import CustomerService from "../../src/services/customer.service";
import init from '../../src/api';
import Boom from '@hapi/boom';

const customerService = new CustomerService();

const payload = {
    "username": "miftah",
    "name": "miftah nurzanah",
    "address": "jakarta",
    "handphone": "021xxxxxxx",
    "password": "12345"
}

const payloads = [{
        "username": "miftah",
        "name": "miftah nurzanah",
        "address": "jakarta",
        "handphone": "021xxxxxxx",
        "password": "12345"
    },
    {
        "username": "nurz",
        "name": "miftah nurzanah",
        "address": "jakarta",
        "handphone": "021xxxxxxx",
        "password": "12345"
    },
    {
        "username": "zanah",
        "name": "miftah nurzanah",
        "address": "jakarta",
        "handphone": "021xxxxxxx",
        "password": "12345"
    }
]
let server;

describe('Find customer', () => {

    beforeEach(async() => {
        server = await init();
        await customerService.customerRepository().clear();
    });

    it('should return an object of customer', async() => {

        const savePeople = await customerService.save(payload);

        const people = await customerService.findOne(savePeople.id);

        expect(people).toMatchObject({
            id: savePeople.id,
            name: savePeople.name,
            username: savePeople.username,
        })
    })

    afterEach(async() => {
        if (server) {
            server.close();
        }
    })
})

describe('Find all customers', () => {

    beforeEach(async() => {
        await customerService.customerRepository().clear();
    })

    it('should return array object customers', async() => {
            let savePeople = [];

            for (let index = 0; index < payloads.length; index++) {
                savePeople.push(await customerService.customerRepository().save(payloads[index]));
            }

            const peoples = await customerService.findAll();
            expect(peoples).toEqual(expect.arrayContaining(savePeople));
        }),
        it('should return array object customers', async() => {
            let savePeople = [];

            for (let index = 0; index < payloads.length; index++) {
                savePeople.push(await customerService.customerRepository().save(payloads[index]));
            }

            const peoples = await customerService.findAll();
            expect(peoples).toHaveLength(savePeople.length);
        })

    afterEach(async() => {
        if (server) {
            server.close();
        }
    })
})

describe('create customer', () => {

    beforeEach(async() => {
        await customerService.customerRepository().clear();
    });

    it('should length plus one when customer created', async() => {
        const lengthBefore = await customerService.findAll();
        await customerService.save(payload);
        const lengthResult = await customerService.findAll();

        expect(lengthResult.length).toBe(lengthBefore.length + 1);
    })

    afterEach(async() => {
        if (server) {
            server.close();
        }
    })
})

describe('update customer', () => {

    beforeEach(async() => {
        await customerService.customerRepository().clear();
    });

    // it('should throw an error when id not found ', async() => {
    //     await expect(customerService.update({ id: null })).rejects.toThrow("Invalid request query input");
    // })

    it('should be update with given id', async() => {
        const people = await customerService.save(payload);
        const updateData = await customerService.update({
            "id": people.id,
            "username": "mm",
            "name": "miftah ",
            "address": "bekasi",
            "handphone": "0898989898",
            "password": "12345"
        })
        const result = await customerService.findOne(people.id);
        expect(result).toMatchObject({
            "id": people.id,
            "username": "mm",
            "name": "miftah ",
            "address": "bekasi",
            "handphone": "0898989898",
            "password": "12345"
        })
    })

    afterEach(async() => {
        if (server) {
            server.close();
        }
    })
})