import CustomerService from '../services/customer.service';

const validate = async(request, username, password) => {
    const user = await new CustomerService().findByUsername(username);

    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await new CustomerService().validPassword(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
}

export default validate;