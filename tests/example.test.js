const contoh = require('../example');

describe('first', () => {
    it('should return positive when parameter positive', () => {
            const result = contoh.contoh(1);
            expect(result).toBe(1);
        }),
        it('should return negative when parameter negative', () => {
            const result = contoh.contoh(-1);
            expect(result).toBe(1);
        }),
        it('should return 0 when parameter 0', () => {
            const result = contoh.contoh(0);
            expect(result).toBe(0);
        })
})