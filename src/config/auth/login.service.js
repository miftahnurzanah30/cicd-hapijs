import { getRepository } from 'typeorm';
import Customer from '../../db/models/customer';
import CustomerService from '../../services/customer.service';

const customerService = new CustomerService();
const Boom = require('@hapi/boom');

export default class LoginService {
    customerRepo() {
        return getRepository(Customer);
    }

    async login(form) {
        const { username, password } = form;

        const cust = await customerService.findByUsername(username);
        if (cust && await customerService.validPassword(password, user.password)) {
            return cust;
        } else {
            throw Boom.unauthorized('invalid login credentials');
        }
    }
}