"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _customer = _interopRequireDefault(require("../../services/customer.service"));

var service = new _customer["default"]();
var customer = {
  method: 'GET',
  path: '/customers',
  handler: function handler(request, h) {
    return service.findAll();
  }
}; // const findOne = {
//     method: 'GET',
//     path: '/customers/{id?}',
//     // handler: (req, h) => {
//     //     const id = req.params;
//     //     return service.findOne(id);
//     // }
//     handler: function(request, h) {
//         const id = request.params.id ? request.params.id : 'id customer not found';
//         return 'ini ';
//     }
// };

var _default = customer;
exports["default"] = _default;