"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var PetCare = function PetCare(id, services, price) {
  (0, _classCallCheck2["default"])(this, PetCare);
  this.id = id;
  this.services = services;
  this.price = price;
};

var _default = PetCare;
exports["default"] = _default;