import { EntitySchema } from 'typeorm';
import PetCare from '../models/petcare';

const PetcareSchema = new EntitySchema({
    name: 'PetCare',
    target: PetCare,
    tableName: 'petcares',
    columns: {
        id: {
            type: 'int',
            generated: true,
            nullable: false,
            primary: true,
        },
        services: {
            type: 'varchar',
            length: 100,
            nullable: false,
        },
        price: {
            type: 'int',
            nullable: false,
        },
    },
    // relations: {
    //     layanan: {
    //         target: 'Transaction',
    //         type: 'many-to-one',
    //         joinColumn: true,
    //     },
    // }
})

export default PetcareSchema;