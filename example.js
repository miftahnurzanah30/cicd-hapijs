const contoh = (number) => {
    return (number >= 0) ? number : -number;
}

module.exports.contoh = contoh;
console.log(contoh(0));