import CustomerSchema from "./schemas/customer.schema";
import PetcareSchema from "./schemas/petcare.schema";
// import TransactionSchema from "./schemas/transaction.schema";

export default [
    CustomerSchema,
    PetcareSchema,
    // TransactionSchema,
]