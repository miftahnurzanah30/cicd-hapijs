import Hapi from '@hapi/hapi';
import configure from './config';
import routes from './routes';
import createConnection from './db/connection';
import validate from './config/auth.validate';

process.on("unhandledRejection", (err) => {
    console.log(err);
    process.exit(1);
});

export default async() => {
    configure();
    const connection = await createConnection();
    const server = Hapi.server({
        port: process.env.APP_PORT,
        host: process.env.APP_HOST
    });

    await server.register(require('@hapi/basic'));
    server.auth.strategy('simple', 'basic', { validate });
    server.route(routes);

    if (connection.isConnected) {
        await server.start();
        console.log(`DATABASE connection name ${process.env.DB_NAME}.`);
        console.log(`server`, process.env.APP_NAME, 'running on ', server.info.uri);
    }

    return server.listener;
};

// const init = async ()=> {

//     //create server
// const server = Hapi.server({
//     port: 3000,
//     host: 'localhost'
// });

//     //routing
// server.route({
//     method: 'GET',
//     path: '/',
//     handler: (request, h) => {
//         return h.response({ statusCode: 200, message: 'Hello World'}).code(200);
//         // return 'Hello World!';
//     }
// });

//     //start server
//     // await server.start();

//     createConnection()
//     .then(async(connection) => {
//         if (connection.isConnected) {
//             console.log('database connected');
//             await server.start();
//             console.log(`server running on port ${server.info.uri}`);

//         process.on("unhandledRejection", (err) => {
//     console.log(err);
//     process.exit(1);
// });
//         }else{
//             throw new Error('unnable to connect database');
//         }
//     })
//     .catch((err) =>{
//         console.log(`error starting application.`);
//         console.error(err);
//     });
// };

// init();