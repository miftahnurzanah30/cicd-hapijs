import { getRepository as repository } from 'typeorm';
import PetCare from '../db/models/petcare';

export default class PetcareService {
    petcareRepo() {
        return repository(PetCare);
    }

    findAll() {
        return this.petcareRepo().find();
    }

    findOne(id) {
        return this.petcareRepo().findOne(id);
    }

    save(PetCare) {
        return this.petcareRepo().save(PetCare);
    }

    delete(id) {
        return this.petcareRepo().delete(id);
    }

    update(data) {
        return this.petcareRepo().save(data);
    }
}