import landing from './landing';
import customers from './customers';
import petcare from './petcares';

const routes = [].concat(landing, customers, petcare);

export default routes;